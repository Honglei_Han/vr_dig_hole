﻿using UnityEngine;
using System.Collections;

public class VRMove : MonoBehaviour {
    [SerializeField]
    protected Transform m_cam;
    protected Vector3 m_pos;
    protected CharacterController m_CharacterController;
	// Use this for initialization
	void Start () {
        m_pos = m_cam.position;
        m_CharacterController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void FixedUpdate()
    {
        Vector3 move = m_cam.position - m_pos;
        move.y = 0;
        m_CharacterController.Move(move*5);

        m_pos = m_cam.position;
    }
}
