﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DigAHole : MonoBehaviour
{
    [System.Serializable]
    public class BirthObject
    {
        [SerializeField]
        public Transform m_BirthPos;
        [SerializeField]
        public GameObject m_Object;

        public GameObject Birth()
        {
            return (GameObject)Instantiate(m_Object, m_BirthPos.position, m_BirthPos.rotation);
        }
    }
    [SerializeField]
    protected List<BirthObject> m_birthObjects;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Player")
            return;

        for (int i = 0; i < m_birthObjects.Count; i++)
        {
            m_birthObjects[i].Birth();
        }
    }
}
